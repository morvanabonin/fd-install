#! /bin/bash

###
# Install Fast Downward
###

if [ ! -d ${HOME}/fast-downward ] && echo "Directory " ${HOME}/fast-downward " DOES NOT exists."
then
    # cria um novo repositório
    mkdir -p ${HOME}/fast-downward

    # create a env var with program directory
    DOWNWARD=${HOME}/fast-downward
    # cria uma variável com o repositório" $DOWNWARD_REPO
else
    # deleta repositório existente
    rm -rf ${HOME}/fast-downward

    # cria um novo repositório
    mkdir -p ${HOME}/fast-downward

    # create a env var with program directory
    DOWNWARD=${HOME}/fast-downward
    # cria uma variável com o repositório" $DOWNWARD_REPO
fi


##
# install mercurial 
# g++
# cmake
# make
# python
echo "Instalando as dependências"
sudo apt-get install mercurial \
g++ \
cmake \
make \
python \
flex \
bison 

# run command and clone fast-downward in mercurial repository
if [ -d ${DOWNWARD} ] && echo "Directory ${DOWNWARD} exists."
then
    hg clone http://hg.fast-downward.org ${DOWNWARD}

    # Optionally check that Fast Downward works:
    cd ${DOWNWARD}
    ./build.py
    ./fast-downward.py ${DOWNWARD}/grid/prob01.pddl \
    #./fast-downward.py ${DOWNWARD_BENCHMARKS}/grid/prob01.pddl \
        --search "astar(lmcut())"
else
  echo "Diretório do repositório DOWNWARD não foi criado!"
fi

###
# Install VAL
###

# run command and clone VAL in github repository
cd ${HOME}
git clone https://github.com/KCL-Planning/VAL.git
if [ -d ${HOME}/VAL ] && echo "Directory" ${HOME}/VAL "exists."
then
    cd VAL
    make clean  # Remove old object files and binaries.
    sed -i 's/-Werror //g' Makefile  # Ignore warnings.
    make

    if [ ! -d /usr/local/bin/validate ] 
    then
        sudo mkdir -p /usr/local/bin/validate
        sudo cp validate /usr/local/bin  # Add binary to a directory on PATH.
    else
        echo "Diretório já existe e cópia já foi efetuada!"
    fi
else
  echo "Diretório não foi criado!"
fi

echo "Fim da instalação!"