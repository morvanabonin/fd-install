# Install Fast Downward

Install based in this doc [_Install Fast Downward_](https://lab.readthedocs.io/en/latest/downward.tutorial.html)

Steps

1. Clone the project
```
    $ git clone git@gitlab.com:morvanabonin/fd-install.git
```

2. Enter into directory
```
    $ cd fd-install
```

3. Turn the install file in a executable file
```
    $ sudo chmod u+x install.sh
```
4. Give permission
```
    $ sudo chmod -R 0700 install.sh
```
5. Run file
```
    $ ./install.sh
```

